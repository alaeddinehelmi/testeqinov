package com.eqinov.recrutement.data;

import java.sql.Date;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

public class ConsommationInstantanne {


	private long id;


	private LocalDateTime date;

	private double value;

	private Consommation consommation;



	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public LocalDateTime getDate() {
		return date;
	}


	public Consommation getConsommation() {
		return consommation;
	}

	public void setConsommation(Consommation consommation) {
		this.consommation = consommation;
	}

	public void setDate(String date) {
		String pattern = "yyyy-MM-dd HH:mm";
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
		LocalDateTime localDateTime = LocalDateTime.from(formatter.parse(date));
		this.date = localDateTime;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public ConsommationInstantanne(String date, double value) {
		super();
		String pattern = "yyyy-MM-dd hh:mm";
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
		LocalDateTime localDateTime = LocalDateTime.from(formatter.parse(date));
		this.date = localDateTime;
		this.value = value;
	}

	public ConsommationInstantanne(long id, LocalDateTime date, double value) {
		super();
		this.id = id;
		this.date = date;
		this.value = value;
	}

	public ConsommationInstantanne() {
		super();
	}


}
