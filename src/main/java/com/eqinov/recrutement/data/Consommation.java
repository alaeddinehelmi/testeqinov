package com.eqinov.recrutement.data;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;


public class Consommation {


	private long id;

	private String site;

	private String unit;


    private Set<ConsommationInstantanne> values = new HashSet<>();

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Set<ConsommationInstantanne> getValues() {
		return values;
	}

	public void setValues(Set<ConsommationInstantanne> values) {
		this.values = values;
	}

	public Consommation(String site, String unit, Set<ConsommationInstantanne> values) {
		super();
		this.site = site;
		this.unit = unit;
		this.values = values;
	}

	public Consommation() {
		super();
	}

	@Override
	public String toString() {
		return "Consommation [site=" + site + ", unit=" + unit + ", values=" + values + "]";
	}



}
