package com.eqinov.recrutement.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.eqinov.recrutement.data.Consommation;


public interface DataPointService {

	public List<?> findConsommationParMois(Integer year);

	public double findConsommationByYear(Integer Year);

	public ResponseEntity<Consommation> getapiConso();

	public void postapiConso();

}
