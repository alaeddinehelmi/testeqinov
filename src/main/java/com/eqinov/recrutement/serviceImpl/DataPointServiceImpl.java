package com.eqinov.recrutement.serviceImpl;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.eqinov.recrutement.data.Consommation;
import com.eqinov.recrutement.data.ConsommationInstantanne;
import com.eqinov.recrutement.data.DataPoint;
import com.eqinov.recrutement.data.Site;
import com.eqinov.recrutement.repository.DataPointRepository;
import com.eqinov.recrutement.repository.SiteRepository;
import com.eqinov.recrutement.service.DataPointService;

@Service
public class DataPointServiceImpl implements DataPointService{


	@Autowired
	private SiteRepository siteRepository;


	@Autowired
	private DataPointRepository dataPointRepository;

	@Override
	public List<?> findConsommationParMois(Integer year) {

		return dataPointRepository.findConsommationParMois(year);
	}

	@Override
	public double findConsommationByYear(Integer year) {
		double consommation = dataPointRepository.findConsommationParAnne(year)/12;
		return consommation;
	}

	@Override
	public ResponseEntity<Consommation> getapiConso() {
		ResponseEntity<Consommation> responseEntity = new RestTemplate().getForEntity(
                "http://localhost:2345/api/conso", Consommation.class);

		return responseEntity;
	}

	@Override
	public void postapiConso() {
		  ResponseEntity<Consommation> responseEntity = getapiConso();
		  Consommation consommation = responseEntity.getBody();

		  Set<ConsommationInstantanne> list = consommation.getValues();

		  Optional<Site> site = siteRepository.findById(1l);

		  list.forEach(conInst->{

			  DataPoint dp = new DataPoint();

			  dp.setSite(site.get());
			  dp.setTime(conInst.getDate());
			  dp.setValue(conInst.getValue());

			  dataPointRepository.save(dp);
		  });

	}

}
