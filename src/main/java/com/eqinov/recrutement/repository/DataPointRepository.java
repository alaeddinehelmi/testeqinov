package com.eqinov.recrutement.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.eqinov.recrutement.data.DataPoint;
import com.eqinov.recrutement.data.DataPointId;
import com.eqinov.recrutement.data.Site;

public interface DataPointRepository  extends JpaRepository<DataPoint, DataPointId> {

	List<DataPoint> findBySite(Site site);

	List<DataPoint> findBySiteAndTimeBetween(Site site, LocalDateTime start, LocalDateTime end);

	DataPoint findTopBySiteOrderByTimeDesc(Site site);

	DataPoint findTopBySiteOrderByTimeAsc(Site site);

	@Query(value = "select sum(value) as value,\n" +
			"concat(year(date),' - ',MONTH(date) ) as Mois_Année,"
			+ "cast(concat(year(date),MONTH(date)) as integer) as Num_mois_année\n" +
			"from\n" +
			"(select to_date (time,'yyyy-mm-dd') as date,Hour(time) as hour, \n" +
			" avg(value) as value ,\n" +

			" from data_point\n" +
			"Group by date,hour, site_id) where year(date)=:year Group by Mois_Année,Num_mois_année order by Num_mois_année asc	", nativeQuery = true)
	List<?> findConsommationParMois(@Param("year") Integer year );

	@Query(value = "select avg(select sum(value) as value from\n" +
			"(select to_date (time,'yyyy-mm-dd') as date,Hour(time) as hour, \n" +
			" avg(value) as value ,\n" +

			" from data_point\n" +
			"Group by date,hour, site_id))from data_point where year(time)=:year	", nativeQuery = true)
	double findConsommationParAnne(@Param("year") Integer year);




}